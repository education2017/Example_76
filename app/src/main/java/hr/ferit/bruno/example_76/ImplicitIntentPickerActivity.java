package hr.ferit.bruno.example_76;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ImplicitIntentPickerActivity extends Activity implements View.OnClickListener {

    private static final String TAG = "Bruno";
    private static final String EMERGENCY_NUMBER = "911";

    Button bBrowse, bDial, bSendSMS;
    EditText etWebAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_implicitintentpicker);
        this.setUpUI();
    }

    private void setUpUI() {
        this.bBrowse = (Button) findViewById(R.id.bBrowse);
        this.bDial = (Button) findViewById(R.id.bDial);
        this.bSendSMS = (Button) findViewById(R.id.bSendSMS);
        this.etWebAddress = (EditText) findViewById(R.id.etWebAddress);

        this.bBrowse.setOnClickListener(this);
        this.bDial.setOnClickListener(this);
        this.bSendSMS.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent implicitIntent = null;
        switch (v.getId())
        {
            case (R.id.bBrowse):
                implicitIntent = new Intent();
                String url = etWebAddress.getText().toString();
                if(url.startsWith("http://") == false){
                    url = "http://" + url;
                }
                Uri uri = Uri.parse(url);
                implicitIntent.setData(uri);
                implicitIntent.setAction(Intent.ACTION_VIEW);
                break;
            case (R.id.bDial):
                implicitIntent = new Intent();
                Uri phoneNumber = Uri.parse("tel:" + EMERGENCY_NUMBER);
                implicitIntent.setAction(Intent.ACTION_DIAL);
                implicitIntent.setData(phoneNumber);
                break;
            case (R.id.bSendSMS):
                implicitIntent = new Intent();
                // This string is stored in the resources file (value="00000"):
                String friendNumber = getResources().getString(R.string.friendPhoneNumber);
                Uri phoneNumberToSMS = Uri.parse("sms:" + friendNumber);
                implicitIntent.setAction(Intent.ACTION_VIEW);
                implicitIntent.setData(phoneNumberToSMS);
                break;
        }

        if(canBeCalled(implicitIntent)){
            startActivity(implicitIntent);
        }
        else{
            Log.d(TAG, "No activity can handle the request.");
        }
    }

    private boolean canBeCalled(Intent implicitIntent) {
        PackageManager packageManager = this.getPackageManager();
        if(implicitIntent.resolveActivity(packageManager) != null){
            return true;
        }
        else{
            return false;
        }
    }
}

